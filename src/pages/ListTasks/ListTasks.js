import React from 'react';
import {useHistory} from 'react-router';

export const ListTasks = () => {
    const history = useHistory();
    const handleEdit = ()=> {
        history.push('/task/12');
    }

    return(
        <div className="row">
            <div className="col-10 offset-1 mt-5">
                <h5>Lista de tareas</h5>
                <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>
                            <button type="button" className="btn btn-outline-secondary" onClick={handleEdit}>Editar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>
                            <button type="button" className="btn btn-outline-secondary" onClick={handleEdit}>Editar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td colSpan="2">Larry the Bird</td>
                        <td>
                            <button type="button" className="btn btn-outline-secondary" onClick={handleEdit}>Editar
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )

}
