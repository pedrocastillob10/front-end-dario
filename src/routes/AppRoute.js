import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import {Navbar} from '../components/iu/Navbar';
import {DetailTask} from "../pages/DetailTask/DetailTask";
import {ListTasks} from "../pages/ListTasks/ListTasks";


export const AppRoute = () => {
    return (
        <Router>
            <div className="main-layout">
                <Navbar />
                <Switch>
                    <Route exact path="/task/:id">
                        <DetailTask />
                    </Route>
                    <Route  exact path="/">
                        <ListTasks />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
